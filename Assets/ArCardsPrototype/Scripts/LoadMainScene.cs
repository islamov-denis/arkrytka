﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

#if PLATFORM_ANDROID
//using UnityEngine.Android;
#endif

public class LoadMainScene : MonoBehaviour
{
    [SerializeField] protected string MainSceneName;
    [SerializeField] private GameObject _loadingLabel;
    [SerializeField] private GameObject _reloabLabel;

    private int _isLoadedFirstTime = 1;
    
    IEnumerator Start()
    {
// #if PLATFORM_ANDROID
//         if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
//         {
//             Permission.RequestUserPermission(Permission.Camera);
//             yield return new WaitForSeconds(0.5f);
//         }
//         
//         _isLoadedFirstTime = PlayerPrefs.GetInt("_isLoadedFirstTime", 1);
//         PlayerPrefs.SetInt("_isLoadedFirstTime", -1);
//         Debug.Log("_isLoadedFirstTime " + _isLoadedFirstTime);
//         
//         if (_isLoadedFirstTime == 1)
//         {
//             _loadingLabel.SetActive(false);
//             _reloabLabel.SetActive(true);
//             
//             yield return new WaitForSeconds(2.5f);
//             Application. Quit();
//         }
// #endif
        yield return new WaitForSeconds(0.5f);
        AsyncOperation async = SceneManager.LoadSceneAsync(MainSceneName);
        yield return async;
    }
}
