﻿using System;
using UnityEngine;
using System.IO;
using System.Collections;

public class CaptureScreenshot : MonoBehaviour
{
    [Space(10)]
    [Header("UI References")]
    [SerializeField]
    protected UnityEngine.UI.Button CaptureScreenshotButton;

    [SerializeField] protected AudioSource SnapAudioSource;
    [SerializeField] protected GameObject Splash;
    
    [SerializeField] protected GameObject[] EnabledGameObjects;
    [SerializeField] protected GameObject[] DisabledGameObjects;

    //     
// #if UNITY_IPHONE
//     [DllImport("__Internal")]
//     private static extern bool saveToGallery(string path);
// #endif

    protected void Awake()
    {
        CaptureScreenshotButton.onClick.AddListener(delegate 
        {
            if (_currentCoroutine == null)
            {
                //_currentCoroutine = StartCoroutine(TakeScreenShot()); 
                _currentCoroutine = StartCoroutine(TakeScreenshotAndSave());
            }
        });
    }

    private Coroutine _currentCoroutine;
    private IEnumerator TakeScreenShot()
    {
        yield return new WaitForEndOfFrame();

        var date = DateTime.Now.ToString("dd_MM_yy_H_mm_ss");
        var screenshotFilename = "arCard" + "_" + date + ".png";

        SnapAudioSource.Play();
        Splash.SetActive(true);
        yield return new WaitForSeconds(.2f);
        Splash.SetActive(false);
        
        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(true);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(false);
        }
        
        yield return new WaitForEndOfFrame();

        var screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        screenShot.ReadPixels( new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenShot.Apply();
        
        //NativeGallery.SaveToGallery(screenShot, "ArCards", screenshotFilename);
        
        yield return new WaitForSeconds(0.2f);
        
        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(false);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(true);
        }
        
        Debug.Log("TakeImage Done");
        _currentCoroutine = null;
    }

    public IEnumerator TakeImage()
    {
        var photoSaved = false;
        var date = DateTime.Now.ToString("dd_MM_yy_H_mm_ss");
        var screenshotFilename = "arCard" + "_" + date + ".png";

        // ANDROID
#if UNITY_ANDROID
        var androidPath = "/../../../../DCIM/" + "ArCards" + "/" + screenshotFilename;
        var path = Application.persistentDataPath + androidPath;
        var pathonly = Path.GetDirectoryName(path);

        if (pathonly != null)
        {
            Directory.CreateDirectory(pathonly);
        }

        SnapAudioSource.Play();
        Splash.SetActive(true);
        yield return new WaitForSeconds(.2f);
        Splash.SetActive(false);
        

        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(true);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(false);
        }

        ScreenCapture.CaptureScreenshot(androidPath);
        var obj = new AndroidJavaClass("com.ryanwebb.androidscreenshot.MainActivity");

        while (!photoSaved)
        {
            photoSaved = obj.CallStatic<bool>("scanMedia", path);

            yield return new WaitForSeconds(.5f);
        }

        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(false);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(true);
        }
#endif
        // IOS
#if UNITY_IPHONE
        string iosPath = Application.persistentDataPath + "/" + screenshotFilename;

        SnapAudioSource.Play();
        Splash.SetActive(true);
        yield return new WaitForSeconds(.2f);
        Splash.SetActive(false);
        
        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(true);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(false);
        }

        ScreenCapture.CaptureScreenshot(screenshotFilename);

        while (!photoSaved)
        {
            //photoSaved = saveToGallery(iosPath);
            yield return new WaitForSeconds(.5f);
        }

        UnityEngine.iOS.Device.SetNoBackupFlag(iosPath);

        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(false);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(true);
        }
#endif

        Debug.Log("TakeImage Done");
    }
    
    private IEnumerator TakeScreenshotAndSave()
    {
        Debug.Log("Take Screenshot And Save");
        var res = NativeGallery.CheckPermission(NativeGallery.PermissionType.Write);
        Debug.Log("res #1: " +  res);
        if (res != NativeGallery.Permission.Granted)
        {
            NativeGallery.OpenSettings();
            res = NativeGallery.RequestPermission(NativeGallery.PermissionType.Write);
            Debug.Log("res #2: " +  res);
            if (res != NativeGallery.Permission.Granted)
            {
                Debug.Log("yield break");
                yield break;
            }
        }
        
        var date = DateTime.Now.ToString("dd_MM_yy_H_mm_ss");
        var screenshotFilename = "arCard" + "_" + date + ".png";

        SnapAudioSource.Play();
        Splash.SetActive(true);
        yield return new WaitForSeconds(.2f);
        Splash.SetActive(false);
        
        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(true);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(false);
        }
        
        yield return new WaitForEndOfFrame();
        
        var ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        Debug.Log("ss");
        ss.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
        ss.Apply();
        
        // Save the screenshot to Gallery/Photos
        var permission = NativeGallery.SaveImageToGallery(ss, "ArCards", screenshotFilename, (success, path ) => Debug.Log( "Media save result: " + success + " " + path));
        Debug.Log("permission");
        Debug.Log( "Permission result: " + permission);

        // To avoid memory leaks
        Destroy( ss );
        
        foreach (var enabledGameObject in EnabledGameObjects)
        {
            enabledGameObject.gameObject.SetActive(false);
        }
        
        foreach (var disabledGameObject in DisabledGameObjects)
        {
            disabledGameObject.gameObject.SetActive(true);
        }
        
        _currentCoroutine = null;
    }
}
