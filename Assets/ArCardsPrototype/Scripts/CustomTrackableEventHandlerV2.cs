using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class CustomTrackableEventHandlerV2 : MonoBehaviour
{
    public enum TrackingStatusFilter
    {
        Tracked,
        Tracked_ExtendedTracked,
        Tracked_ExtendedTracked_Limited
    }

    public TrackingStatusFilter StatusFilter = TrackingStatusFilter.Tracked_ExtendedTracked_Limited;

    private TrackableBehaviour _trackableBehaviour;
    
    private TrackableBehaviour.Status _previousStatus;
    private TrackableBehaviour.Status _newStatus;
    
    private TrackableBehaviour.StatusInfo _previousStatusInfo;
    private TrackableBehaviour.StatusInfo _newStatusInfo;
    
    private bool _callbackReceivedOnce = false;

    protected virtual void Start()
    {
        _trackableBehaviour = GetComponent<TrackableBehaviour>();

        if (_trackableBehaviour)
        {
            _trackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStatusChanged);
            _trackableBehaviour.RegisterOnTrackableStatusInfoChanged(OnTrackableStatusInfoChanged);
        }
    }

    protected virtual void OnDestroy()
    {
        if (_trackableBehaviour)
        {
            _trackableBehaviour.UnregisterOnTrackableStatusInfoChanged(OnTrackableStatusInfoChanged);
            _trackableBehaviour.UnregisterOnTrackableStatusChanged(OnTrackableStatusChanged);
        }
    }

    void OnTrackableStatusChanged(TrackableBehaviour.StatusChangeResult statusChangeResult)
    {
        _previousStatus = statusChangeResult.PreviousStatus;
        _newStatus = statusChangeResult.NewStatus;

        Debug.LogFormat("Trackable {0} {1} -- {2}",
            _trackableBehaviour.TrackableName,
            _trackableBehaviour.CurrentStatus,
            _trackableBehaviour.CurrentStatusInfo);

        HandleTrackableStatusChanged();
    }

    void OnTrackableStatusInfoChanged(TrackableBehaviour.StatusInfoChangeResult statusInfoChangeResult)
    {
        _previousStatusInfo = statusInfoChangeResult.PreviousStatusInfo;
        _newStatusInfo = statusInfoChangeResult.NewStatusInfo;
        
        HandleTrackableStatusInfoChanged();
    }

    protected virtual void HandleTrackableStatusChanged()
    {
        if (!ShouldBeRendered(_previousStatus) &&
            ShouldBeRendered(_newStatus))
        {
            TrackingFound();
        }
        else if (ShouldBeRendered(_previousStatus) &&
                 !ShouldBeRendered(_newStatus))
        {
            TrackingLost();
        }
        else
        {
            if (!_callbackReceivedOnce && !ShouldBeRendered(_newStatus))
            {
                // This is the first time we are receiving this callback, and the target is not visible yet.
                // --> Hide the augmentation.
                TrackingLost();
            }
        }

        _callbackReceivedOnce = true;
    }

    protected virtual void HandleTrackableStatusInfoChanged()
    {
        if (_newStatusInfo == TrackableBehaviour.StatusInfo.WRONG_SCALE)
        {
            Debug.LogErrorFormat("The target {0} appears to be scaled incorrectly. " + 
                                 "This might result in tracking issues. " + 
                                 "Please make sure that the target size corresponds to the size of the " + 
                                 "physical object in meters and regenerate the target or set the correct " +
                                 "size in the target's inspector.", _trackableBehaviour.TrackableName);
        }
    }

    protected bool ShouldBeRendered(TrackableBehaviour.Status status)
    {
        if (status == TrackableBehaviour.Status.DETECTED ||
            status == TrackableBehaviour.Status.TRACKED)
        {
            return true;
        }

        if (StatusFilter == TrackingStatusFilter.Tracked_ExtendedTracked)
        {
            if (status == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                return true;
            }
        }

        if (StatusFilter == TrackingStatusFilter.Tracked_ExtendedTracked_Limited)
        {
            if (status == TrackableBehaviour.Status.EXTENDED_TRACKED ||
                status == TrackableBehaviour.Status.LIMITED)
            {
                return true;
            }
        }

        return false;
    }

    protected virtual void TrackingFound()
    {
        if (_trackableBehaviour)
        {
            var rendererComponents = _trackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = _trackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = _trackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;
        }
        
    }

    protected virtual void TrackingLost()
    {
        if (_trackableBehaviour)
        {
            var rendererComponents = _trackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = _trackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = _trackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;
        }
    }
}
