﻿using UnityEngine;
using System.Collections;

public class SpawnParticle : MonoBehaviour
{
    [SerializeField]
    protected ParticleSystem ParticleSystemRef;

    public void Play()
    {
        Debug.Log(gameObject.name + " spawn " + ParticleSystemRef.name);
        ParticleSystemRef.gameObject.SetActive(true);
    }

    public void Stop()
    {
        Debug.Log(gameObject.name + " despawn " + ParticleSystemRef.name);
        ParticleSystemRef.gameObject.SetActive(false);
    }
}
