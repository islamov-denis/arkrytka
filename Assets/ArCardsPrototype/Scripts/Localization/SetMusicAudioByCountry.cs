﻿using System.Collections;
using UnityEngine;

public class SetMusicAudioByCountry : MonoBehaviour
{
    [System.Serializable]
    public class MusicByCode
    {
        public string Code = "SA";
        public AudioClip Clip;
    }

    [SerializeField] private MusicByCode[] _musicByCodes;
    [SerializeField] private CustomTrackableEventHandler[] _customTrackableEventHandlers;

    [Space(10)]
    [SerializeField] private  CountryIdentifier _countryIdentifier;

    private void Awake()
    {
        _countryIdentifier.OnCountryCode.AddListener(CheckCountryCode);
        _countryIdentifier.OnCountryCodeByGps.AddListener(CheckCountryCodeByGps);
    }

    private void OnDestroy()
    {
        _countryIdentifier.OnCountryCode.RemoveListener(CheckCountryCode);
        _countryIdentifier.OnCountryCodeByGps.RemoveListener(CheckCountryCodeByGps);
    }

    private void CheckCountryCode(string value)
    {
        Debug.LogFormat("CheckCountryCode {0}", value);
                
        foreach (var musicByCode in _musicByCodes)
        {
            if (musicByCode.Code != _countryIdentifier.CountryCode)
            {
                continue;
            }
                
            foreach (var customTrackableEventHandler in _customTrackableEventHandlers)
            {
                Debug.LogFormat("Set {0} to {1}",musicByCode.Clip, customTrackableEventHandler.MusicAudioClip);
                customTrackableEventHandler.MusicAudioClip = musicByCode.Clip;
            }
        }
    }

    private void CheckCountryCodeByGps(string value)
    {
        Debug.LogFormat("CheckCountryCodeByGps {0}", value);
        
        foreach (var musicByCode in _musicByCodes)
        {
            if (musicByCode.Code != _countryIdentifier.CountryCodeByGps)
            {
                continue;
            }
                
            foreach (var customTrackableEventHandler in _customTrackableEventHandlers)
            {
                Debug.LogFormat("Set {0} to {1}",musicByCode.Clip, customTrackableEventHandler.MusicAudioClip);
                customTrackableEventHandler.MusicAudioClip = musicByCode.Clip;
            }
        }
    }
}
