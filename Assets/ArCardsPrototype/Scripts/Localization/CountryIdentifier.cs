﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;

public class CountryIdentifier : MonoBehaviour
{

    private static readonly Dictionary<SystemLanguage, string> COUTRY_CODES =
        new Dictionary<SystemLanguage, string>()
        {
            {SystemLanguage.Afrikaans, "ZA"},
            {SystemLanguage.Arabic, "IR"},
            {SystemLanguage.Basque, "US"},
            {SystemLanguage.Belarusian, "BY"},
            {SystemLanguage.Bulgarian, "BJ"},
            {SystemLanguage.Catalan, "ES"},
            {SystemLanguage.Chinese, "CN"},
            {SystemLanguage.Czech, "HK"},
            {SystemLanguage.Danish, "DK"},
            {SystemLanguage.Dutch, "BE"},
            {SystemLanguage.English, "US"},
            {SystemLanguage.Estonian, "EE"},
            {SystemLanguage.Faroese, "FU"},
            {SystemLanguage.Finnish, "FI"},
            {SystemLanguage.French, "FR"},
            {SystemLanguage.German, "DE"},
            {SystemLanguage.Greek, "JR"},
            {SystemLanguage.Hebrew, "IL"},
            {SystemLanguage.Icelandic, "IS"},
            {SystemLanguage.Indonesian, "ID"},
            {SystemLanguage.Italian, "IT"},
            {SystemLanguage.Japanese, "JP"},
            {SystemLanguage.Korean, "KR"},
            {SystemLanguage.Latvian, "LV"},
            {SystemLanguage.Lithuanian, "LT"},
            {SystemLanguage.Norwegian, "NO"},
            {SystemLanguage.Polish, "PL"},
            {SystemLanguage.Portuguese, "PT"},
            {SystemLanguage.Romanian, "RO"},
            {SystemLanguage.Russian, "RU"},
            {SystemLanguage.SerboCroatian, "SP"},
            {SystemLanguage.Slovak, "SK"},
            {SystemLanguage.Slovenian, "SI"},
            {SystemLanguage.Spanish, "ES"},
            {SystemLanguage.Swedish, "SE"},
            {SystemLanguage.Thai, "TH"},
            {SystemLanguage.Turkish, "TR"},
            {SystemLanguage.Ukrainian, "UA"},
            {SystemLanguage.Vietnamese, "VN"},
            {SystemLanguage.ChineseSimplified, "CN"},
            {SystemLanguage.ChineseTraditional, "CN"},
            {SystemLanguage.Unknown, "US"},
            {SystemLanguage.Hungarian, "HU"},
        };

    private static string ToCountryCode(SystemLanguage language)
    {
        string result;
        return COUTRY_CODES.TryGetValue(language, out result) ? result : COUTRY_CODES[SystemLanguage.Unknown];
    }

    private float _latitude;
    private float _longitude;
    private float _altitude;
    private float _horizontalAccuracy;
    private double _timestamp;

    public string CountryCode { get; private set; }
    public string CountryCodeByGps { get; private set; }
    public bool CountryFound  { get; private set; }
    
    public class UnityEventWitString : UnityEvent<string>
    {
        
    }

    public UnityEventWitString OnCountryCode = new UnityEventWitString();
    public UnityEventWitString OnCountryCodeByGps = new UnityEventWitString();
    
    private IEnumerator Start()
    {
        CountryCode = ToCountryCode(Application.systemLanguage);
        OnCountryCode.Invoke(CountryCode);
        
        if (!Input.location.isEnabledByUser)
        {
            yield break;
        }

        Input.location.Start(1,0.1f);

        var maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing &&
               maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }

        print("Location: " + Input.location.lastData.latitude + " " +
              Input.location.lastData.longitude + " " +
              Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " +
              Input.location.lastData.timestamp);

        _latitude = Input.location.lastData.latitude;
        _longitude = Input.location.lastData.longitude;
        _altitude = Input.location.lastData.altitude;
        _horizontalAccuracy = Input.location.lastData.horizontalAccuracy;
        _timestamp = Input.location.lastData.timestamp;

        Input.location.Stop();
        
        var www = new WWW("https://maps.googleapis.com/maps/api/geocode/xml?latlng=" +
                          _latitude + "," + _longitude + "&sensor=true");
        yield return www;

        if (www.error != null)
        {
            print(www.error);
            yield break;
        }
        
        var reverseGeocodeResult = new XmlDocument();
        reverseGeocodeResult.LoadXml(www.text);
        
        if (reverseGeocodeResult.GetElementsByTagName("status").Item(0).ChildNodes.Item(0).Value != "OK")
        {
            yield break;
        }
        
        foreach(XmlNode eachAdressComponent in reverseGeocodeResult.GetElementsByTagName("result").Item(0).ChildNodes)
        {
            if (eachAdressComponent.Name != "address_component")
            {
                continue;
            }
            
            foreach(XmlNode eachAddressAttribute in eachAdressComponent.ChildNodes)
            {
                if (eachAddressAttribute.Name == "short_name")
                {
                    CountryCodeByGps = eachAddressAttribute.FirstChild.Value;
                }

                if (eachAddressAttribute.Name == "type" && eachAddressAttribute.FirstChild.Value == "country")
                {
                    CountryFound = true;
                }
            }


            if (!CountryFound)
            {
                continue;
            }
            
            OnCountryCodeByGps.Invoke(CountryCodeByGps);
            break;
        }
    }

//    private void OnGUI()
//    {
//        GUILayout.Label("_latitude: " + _latitude + "\n_longitude: " + _longitude + 
//                        "\nCountryCode: " + CountryCode + "\nCountryCodeByGps: " + CountryCodeByGps +"\nCountryFound: " + CountryFound);
//    }
}
